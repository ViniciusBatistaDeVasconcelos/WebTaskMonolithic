package br.com.webtask.aula.controller.service;

import java.util.ArrayList;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.transaction.Transactional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.webtask.aula.domain.model.Task;
import br.com.webtask.aula.domain.model.UserClient;

//@ExtendWith(SpringExtension.class)
//@ActiveProfiles("test")
//@DataJpaTest
@SpringBootTest
@Transactional
class UserServiceTest {

	@Autowired
	UserService service;

	UserClient user1, user2;

	@BeforeEach
	void setUp() {
		System.out.println("####################################### CRIANDO BASE - UserService!");

		user1 = new UserClient(1l, "Maria", "11111111111", "example1@example.com", "111", true, new ArrayList<Task>());
		user2 = new UserClient(2l, "Marcos", "22222222222", "example2@example.com", "222", true, new ArrayList<Task>());

		service.salvar(user1);
		service.salvar(user2);
	}

	@AfterEach
	void tearDown() {
		System.out.println("####################################### DESTRUINDO BASE - UserService!");
	}

	@Test
	void testBuscarUsuario1() {

		try {
			UserClient user = service.buscarUsuario(1l);
			System.out.println(user.getName());
		} catch (Exception e) {
			Assertions.fail("Erro ao localizar objeto!");
		}

	}

	@Test
	void testBuscarUsuario2() {

		try {
			UserClient user = service.buscarUsuario(2l);
			System.out.println(user.getName());
		} catch (Exception e) {
			Assertions.fail("Erro ao localizar objeto!");
		}

	}

}
