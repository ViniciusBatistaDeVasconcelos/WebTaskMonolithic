package br.com.webtask.aula.controller.service;


import java.time.LocalDate;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import br.com.webtask.aula.domain.model.Task;
import br.com.webtask.aula.domain.repo.TaskRepo;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
class TaskServiceTest {

	@InjectMocks
	private TaskService tasks;

	@MockBean
	private TaskRepo tasksRepo;

	@BeforeEach
	void setUp() {
		System.out.println("####################################### CRIANDO BASE - TaskService!");
		MockitoAnnotations.initMocks(this);
	}

	@AfterEach
	void tearDown() throws Exception {
		System.out.println("####################################### DESTRUINDO BASE - TaskService!");
	}

	@Test
	void testSomeMethod() throws Exception {
		// cenário

		Task t = new Task(1l, "", LocalDate.now().minusDays(2), null, null);
		Task t1 = new Task(1l, "", LocalDate.now().minusDays(2), LocalDate.now(), null);

		Mockito.when(tasksRepo.save(Mockito.any(Task.class))).thenReturn(t1);

		// execução
		try {

			Task tReturned = tasks.finalizar(t);
			Assertions.fail("Deveria gerar um erro!");

		} catch (Exception e) {
			// verificação
			Assertions.assertTrue(true);
		}
	}

}
