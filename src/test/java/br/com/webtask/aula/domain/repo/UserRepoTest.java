package br.com.webtask.aula.domain.repo;


import java.util.ArrayList;
import java.util.Optional;


import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import br.com.webtask.aula.domain.model.Task;
import br.com.webtask.aula.domain.model.UserClient;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@DataJpaTest
class UserRepoTest {

	@Autowired
	private UserRepo users;

	UserClient user1, user2;

	@BeforeEach
	public void init() {

		System.out.println("####################################### CRIANDO BASE - UserRepo!");

		user1 = new UserClient(1l, "Vinícius", "11111111111", "example1@example.com", "123", true,
				new ArrayList<Task>());
		user2 = new UserClient(2l, "Claudemir", "22222222222", "example2@example.com", "456", true,
				new ArrayList<Task>());
		users.save(user1);
		users.save(user2);

	}

	@AfterEach
	public void destroy() {

		System.out.println("####################################### DESTRUINDO BASE - UserRepo!");
		users.delete(user1);
		users.delete(user2);
	}

	@Test
	public void testSomeMethod1() {

		// cenário

		// execução

		Optional<UserClient> userOpt1 = users.findByName("Vinícius");
		
		Optional<UserClient> userOpt2 = users.findByName("Claudemir");

		// verificação

		Assertions.assertNotNull(userOpt1.get());
		Assertions.assertNotNull(userOpt2.get());
	}

	@Test
	public void testSomeMethod2() {

		// cenário

		// verificação
		
		// execução
		
		Assertions.assertThrows(DataIntegrityViolationException.class, () -> {
			
	        users.saveAndFlush(new UserClient(3l, "Ana", "33333333333", "example1@example.com", "789", true,
					new ArrayList<Task>()));
	    });

	}
}
