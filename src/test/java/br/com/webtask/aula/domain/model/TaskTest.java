package br.com.webtask.aula.domain.model;

import java.time.LocalDate;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class TaskTest {

	
	@Test
	public void verificarSeUmaTaskAtrasadaEstaNesteStatus() {
		Task t = new Task(1l, "Estudar", LocalDate.now().minusDays(2), null, null);
		EStatus status = t.getStatus();
		Assertions.assertEquals(EStatus.ATRASADO, status);
	}

	@Test
	public void verificarSeUmaTaskNovaEstaNesteStatus() {
		Task t = new Task(1l, "Estudar", LocalDate.now(), null, null);
		EStatus status = t.getStatus();
		Assertions.assertEquals(EStatus.NOVO, status);
	}

	@Test
	public void verificarSeUmaTaskAtrasadaConcluidaEstaNesteStatus() {
		Task t = new Task(1l, "Estudar", LocalDate.now().minusDays(2), LocalDate.now(), null);
		EStatus status = t.getStatus();
		Assertions.assertEquals(EStatus.CONCLUIDO_ATRASADO, status);
	}

	@Test
	public void verificarSeUmaTaskConcluidaEstaNesteStatus() {
		Task t = new Task(1l, "Estudar", LocalDate.now().minusDays(2), LocalDate.now().minusDays(2), null);
		EStatus status = t.getStatus();
		Assertions.assertEquals(EStatus.CONCLUIDO_PRAZO, status);
	}

}
