package br.com.webtask.aula.selenium;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
class FunctionalScreenTest4 {

	@LocalServerPort
	private int porta;
	
	private WebDriver driver;
	
	@BeforeEach
	void setUp() {
		System.setProperty("webdriver.gecko.driver", "geckodriver.exe");
		driver = new FirefoxDriver();
	}

	@AfterEach
	void tearDown() {
		driver.quit();
	}

	@Test
	void test() {
		driver.get("http://localhost:"+porta+"/login");
		driver.manage().window().setSize(new Dimension(1352, 616));
		
		driver.findElement(By.id("username")).sendKeys("123");
		driver.findElement(By.id("password")).sendKeys("123");
		
		driver.findElement(By.cssSelector(".login100-form-btn")).click();
		
		driver.findElement(By.cssSelector("li.nav-item:nth-child(6) > a:nth-child(1) > span:nth-child(2)")).click();
	
		Assertions.assertThat(driver.findElement(By.cssSelector(".login100-form-title")).getText()).isEqualTo("LOGIN");
	}

}
